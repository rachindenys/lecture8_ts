import IFighterDetailsInterface from "./Interfaces/IFighterDetailsInterface";
import { hideFightModal, showFightModal } from "./modals/fightModal";

export async function fight(firstFighter: IFighterDetailsInterface, secondFighter: IFighterDetailsInterface) : Promise<IFighterDetailsInterface> {
    const firstStartParams = { health: firstFighter.health, defense: firstFighter.defense };
    const secondStartParams = { health: secondFighter.health, defense: secondFighter.defense };  
    while(true){
          let damage1: number = getDamage(firstFighter, secondFighter);
          damage1 > 0 ? secondFighter.health -= damage1 : 0;
          if (secondFighter.health <= 0){
            firstFighter.health = firstStartParams.health;
            secondFighter.health = secondStartParams.health;
            firstFighter.defense = firstStartParams.defense;
            secondFighter.defense = secondStartParams.defense;
            hideFightModal();
            return firstFighter;
          }
          hideFightModal();
          showFightModal(firstFighter, secondFighter, damage1, true);
          await delay(1000);
          let damage2: number = getDamage(secondFighter, firstFighter);
          damage2 > 0 ? firstFighter.health -= damage2 : 0;
          if (firstFighter.health <= 0){
            firstFighter.health = firstStartParams.health;
            secondFighter.health = secondStartParams.health;
            firstFighter.defense = firstStartParams.defense;
            secondFighter.defense = secondStartParams.defense;
            hideFightModal();
            return secondFighter;
          }
          hideFightModal();
          showFightModal(firstFighter, secondFighter, damage2, false);
          await delay(1000);
          firstFighter.name === 'Trump' ? firstFighter.defense += 1 : 0;
          secondFighter.name === 'Trump' ? secondFighter.defense += 1 : 0;
    }
  }
  
  export function getDamage(attacker: IFighterDetailsInterface, enemy: IFighterDetailsInterface) {
     const damage: number = getHitPower(attacker) - getBlockPower(enemy);
     return damage 
  }
  
  export function getHitPower(fighter: IFighterDetailsInterface): number {
     const power: number = fighter.attack * (Math.random()+1);
     return power;
  }
  
  export function getBlockPower(fighter: IFighterDetailsInterface): number {
     const power: number = fighter.defense * (Math.random()+1);
     return power;
  }
  
  function delay(ms: number) : Promise<void>
  {
    return new Promise(resolve => setTimeout(resolve, ms));
  }