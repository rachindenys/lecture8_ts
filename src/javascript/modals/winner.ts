import { createElement } from "../helpers/domHelper";
import IDictionary from "../Interfaces/IDictionary";
import IFighterInterface from "../Interfaces/IFighterInterface";
import { showModal } from "./modal";

export  function createWinnerModal(fighter: IFighterInterface) : HTMLElement{
  const { name, source } = fighter;
  const winnerModal : HTMLElement = createElement({ tagName: 'div', className: 'modal-body-winner' });
  const nameElement : HTMLElement = createElement({ tagName: 'div', className: 'fighter-name' });
  const attributes: IDictionary = { src: source };
  const imgElement: HTMLElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  nameElement.innerText = name;
  winnerModal.append(nameElement);
  winnerModal.append(imgElement);
  
  return winnerModal;
  }

  export  function showWinnerModal(fighter : IFighterInterface) {
    const title : string = 'WINNER!';
    const bodyElement: HTMLElement = createWinnerModal(fighter);
    showModal({ title, bodyElement });
  }