import { callApi } from '../helpers/apiHelper';
import CrudEnum from '../Interfaces/CrudEnum';
import IFighterDetailsInterface from '../Interfaces/IFighterDetailsInterface';
import IFighterInterface from '../Interfaces/IFighterInterface';

export async function getFighters(): Promise<IFighterInterface[]> {
  try {
    const endpoint: string = 'fighters.json';
    const apiResult: IFighterInterface[] = <IFighterInterface[]>await callApi(endpoint, CrudEnum.GET);
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id : string): Promise<IFighterDetailsInterface> {
    try{
        const endpoint: string = `details/fighter/${id}.json`;
        const apiResult: IFighterDetailsInterface = <IFighterDetailsInterface>await callApi(endpoint, CrudEnum.GET);

        return apiResult;
    } catch (error) {
        throw error;
    }
}

