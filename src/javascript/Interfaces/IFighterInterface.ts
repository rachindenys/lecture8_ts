

type IFighterInterface = {
    _id: string,
    name: string,
    source: string
};

export default IFighterInterface;