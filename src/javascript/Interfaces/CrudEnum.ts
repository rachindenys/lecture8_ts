


enum CrudEnum{
    CREATE = "CREATE",
    GET = "GET",
    UPDATE = "UPDATE",
    DELETE = "DELETE"
}

export default CrudEnum;