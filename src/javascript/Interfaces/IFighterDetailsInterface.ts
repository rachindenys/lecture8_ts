
interface IFighterDetailsInterface{
    _id: string,
    name: string,
    health: number,
    attack: number,
    defense: number,
    source: string
}

export default IFighterDetailsInterface;