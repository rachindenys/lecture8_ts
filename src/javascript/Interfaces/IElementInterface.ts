import IDictionary from "./IDictionary";

interface IElementInterface{
    tagName: string,
    className?: string,
    attributes?: IDictionary
}

export default IElementInterface;